//filename: myhttp.c
//author: Katherine Mann & Taryn Musgrave
//purpose: ICT374 Group Assignment Client Program

#include <stdio.h>
#include <string.h> //strcpy, strcmp
#include <stdlib.h> //exit
#include <netinet/in.h> //struct sockaddr_in, htons, htonl, INADDR_ANY
#include  <netdb.h> //struct hostent, gethostbyname()  
#include <errno.h> //errno, EINTR, perror
#include <ctype.h> // toupper()


#define SERVER_TCP_PORT 80
#define BUFFER 256

int indexOf(char *str, char *searchFor, int startIndex){
	if(startIndex > strlen(str))
		return -1;

	char *offset = str + startIndex;
	const char *found = strstr(offset, searchFor);
	return !found ? -1 : found - str;
}

int main(int argc, char *argv[]){
	int i, sd, contentOnly = 1;
	char method[6] = "get"; //defaults to GET
	char url[BUFFER];
	unsigned short port = SERVER_TCP_PORT;
	struct sockaddr_in serverAddress;
	struct hostent *hp;
	
	//assess arguments for valid option indicator & url
	if(argc == 1){
		//no arguments provided :(
		printf("Usage: myhttp [ -m <method> ] [ -a ] <url>\n"); 
		exit(1);
	
	}else if(argc == 2){ //only providing url, all others defaults: GET, display content of response only
		strcpy(url, argv[1]);
	}else{	
		for(i=1;i<argc-1;i++){ //checkem, last one has to be url
			if(strcmp(argv[i], "-m") == 0){
				//method supplied, HEAD or TRACE
				if(strcmp(argv[i+1], "head") == 0 || strcmp(argv[i+1], "trace") == 0 || strcmp(argv[i+1], "get") == 0){
					strcpy(method, argv[i+1]);
				}else{
					printf("Error: invalid method\n");
					exit(1);
				}
			} else if (strcmp(argv[i], "-a") == 0){
				//display entire response message including both headers and content
				contentOnly = 0;
			}
		}
		strcpy(url, argv[argc-1]);  //what if they don't give a url?
	}

	// Parse url into host, port, abs_uri
	int urlLen = strlen(url);

	int hostStart = indexOf(url, "http://", 0) == -1 ? 0 : 7;
	int colon = indexOf(url, ":", hostStart);
	int slash = indexOf(url, "/", hostStart);
	int query = indexOf(url, "?", hostStart);

	int hostEnd, resourceUriStart;
	if(colon != -1){
		hostEnd = colon;
		int portStart = colon + 1;
		int portEnd = slash == -1
			? query == -1
				? urlLen
				: query
			: slash;
		resourceUriStart = portEnd;

		char portStr[5];
		strncpy(portStr, url + portStart, portEnd - portStart);
		port = atoi(portStr);
	} else {
		port = SERVER_TCP_PORT;

		resourceUriStart = hostEnd = slash == -1
			? query == -1
				? urlLen
				: query
			: slash;
	}

	//set up socket
	//host address
	memset(&serverAddress, '\0', sizeof(serverAddress)); //instead of bzero
	serverAddress.sin_family = AF_INET;
	serverAddress.sin_port = htons(port);

	char host[255];
	char resourceUri[1024];
	strncpy(host, url + hostStart, hostEnd - hostStart);
	// printf("Host was: %s\n", host);

	strcpy(resourceUri, url + resourceUriStart);
	if(strlen(resourceUri) == 0){
		resourceUri[0] = '/';
	}
	// printf("ResourceURI was: %s\n", resourceUri);


	if((hp = gethostbyname(host)) == NULL){
		printf("host %s is not found \n", host);
		exit(1);
	}
	serverAddress.sin_addr.s_addr = *(u_long *) hp->h_addr;

	
	//call server
	if((sd = socket(PF_INET, SOCK_STREAM, 0)) < 0 ){
		perror("Error: socket");
		exit(1);
	}
	
	if(connect(sd, (struct sockaddr *) &serverAddress, sizeof(serverAddress)) < 0){
		perror("Error: client connect"); //CONNECTION REFUSED
		exit(1);
	}

	// Upper method
	for(int i = 0; i < strlen(method); i++){
		method[i] = toupper((unsigned char)method[i]);
	}

	char reqMsg[1024];
	char *httpVersion = "HTTP/1.1";
	char firstLine[100];
	char secondLine[100];
	sprintf(firstLine, "%s %s %s", method, resourceUri, httpVersion);
	if(port == SERVER_TCP_PORT)
		sprintf(secondLine, "Host: %s", host);
	else
		sprintf(secondLine, "Host: %s:%d", host, port);

	sprintf(reqMsg, "%s\r\n%s\r\n\r\n", firstLine, secondLine);

printf("Request message: %s", reqMsg);
fflush(stdout);

	// receive reply and display to screen
	send(sd, reqMsg, strlen(reqMsg), 0);

	char buffer[1024];
	ssize_t messageSize;
	if(contentOnly){
		printf("Message content was:\n");

		int startedContent, endOfHeader;
		while((messageSize = recv(sd, buffer, 1024, 0)) > 0){
			if(!startedContent) {
				endOfHeader = indexOf(buffer, "\r\n\r\n", 0);
				if(endOfHeader >= 0){
					startedContent = 1;
					printf("%s", buffer + endOfHeader + 4);
					continue;
				}
			}

			if(messageSize < 1024)
				buffer[messageSize] = '\0';

			if(startedContent) {
				printf("%s", buffer);
			}
		}
	} else{

		printf("Message was: \n");
		while((messageSize = recv(sd, buffer, 1024, 0)) > 0){
			if(messageSize < 1024)
				buffer[messageSize] = '\0';
	
			printf("%s", buffer);
		}
	}
	if(messageSize == -1) {
		perror("Receive error");
		exit(1);
	}

	printf("\nThanks and have a nice day!\n");
}
