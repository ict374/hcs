//filename: token.h
//author: KM retrofitted from HX
//date: 31/10/2017

#include <stdlib.h>

#define MAX_NUM_TOKENS 100

//returns number of tokens or -1 if token array too small
int tokenise(char line[], char *token[], char sep[]);
