# makefile for ICT374 Group Assignment 
# adjust as more things come in

all: myhttpd myhttp

myhttpd: myhttpd.o token.o
	gcc myhttpd.o token.o -o myhttpd
	
myhttp: myhttp.o
	gcc myhttp.o -o myhttp

myhttp.o: myhttp.c
	gcc -c myhttp.c

myhttpd.o: myhttpd.c token.h
	gcc -c myhttpd.c 
	
token.o: token.c token.h
	gcc -c token.c

test: all
	./myhttp http://www.cplusplus.com:80/reference/cstring/strcpy/

testDashA: all
	./myhttp -a http://www.cplusplus.com:80/reference/cstring/strcpy/

curlTrace: all
	curl -v -X TRACE http://localhost:8000
	
clean:
	rm *.o
