//filename: token.c
//author: KM
//date: 31/10/2017

#include <string.h>
#include "token.h"

int tokenise (char line[], char *token[], char sep[])
{
      char *tk;
      int i=0;

      tk = strtok(line, sep);
      token[i] = tk;

      while (tk != NULL) {

          ++i;
          if (i>=MAX_NUM_TOKENS) {
              i = -1;
              break;
          }

          tk = strtok(NULL, sep);
          token[i] = tk;
      }
      return i;
}