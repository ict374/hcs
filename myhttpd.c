//filename: myhttpd.c
//author: Katherine Mann & Taryn Musgrave
//purpose: ICT374 Group Assignment Server Program

#include <stdio.h>
#include <string.h> //strcmp
#include <stdlib.h> //exit
#include <signal.h> //sigaction
#include <sys/types.h> //pid_t
#include <sys/wait.h> //waitpid, WNOHAND
#include <netinet/in.h> //struct sockaddr_in, htons, htonl, INADDR_ANY
#include <sys/stat.h> //umask, stat
#include <unistd.h> //fork etc
#include <errno.h> //errno, EINTR, perror
#include "token.h" //to parse request
#include <dirent.h> //to return directory listing
#include <time.h> // to construct date header


#define HTTP_VERSION "HTTP/1.1"
#define TCP_PORT 8000 
#define LOG_FILE "./myhttdp.log"
#define PREFORKS 5
#define BUFFER 256

typedef struct mime_type
{
	char* ext;
	char* type;
}mime_type;

//globals
char docRoot[BUFFER] = "\0"; 
int numChild = 0;
int preforks;
FILE* logF = NULL;
mime_type *mt;
int numMT;


//Functions
int findFile(char* dir, char* fn);
char* getHDate();
void daemon_init(void);
void claim_children();
void processRequest(int client);
void logToFile(char *method, char *host, char *resourceRequest, char *statusCode);

int main(int argc, char *argv[], char *envp[]){
	unsigned short port;
	//char docRoot[BUFFER] = "\0"; moved to global
	char logFile[BUFFER] = "\0";
	char mimeTypeFile[BUFFER] = "\0";
	//int preforks; moved to global
	int sd, nsd; //socket descriptors
	struct sockaddr_in serverAddress;
	struct sockaddr_in clientAddress;
	socklen_t clientAddressLength;
	int i;
	pid_t pid;
	
	
	
	
	printf("setting defaults\n");
	
	//set defaults
	port = TCP_PORT;
	strcpy(docRoot, getenv("PWD"));  
	strcpy(logFile, LOG_FILE);
	preforks = 5;

	numMT = 10;
	mt = malloc(numMT * sizeof(mime_type));
	mt[0].ext =	".htm";
	mt[0].type = "text/html";
	mt[1].ext = ".html";
	mt[1].type = "text/html";
	mt[2].ext =	".txt";
	mt[2].type = "text/plain";
	mt[3].ext =	".c";
	mt[3].type = "text/plain"; 
	mt[4].ext =	".h";
	mt[4].type = "text/plain";
	mt[5].ext =	".jpg";
	mt[5].type = "image/jpeg";
	mt[6].ext =	".jpeg";
	mt[6].type = "image/jpeg";
	mt[7].ext =	".gif";
	mt[7].type = "image/gif";
	mt[8].ext = ".bmp";
	mt[8].type = "image/bmp";
	mt[9].ext = ".png";
	mt[9].type = "image/png";

	
	printf("port: %d\n", port);
	printf("docRoot: %s\n", docRoot);
	printf("logFile: %s\n", logFile);
	printf("preforks: %d\n", preforks);
	
	printf("checking the % d arguments\n", argc);
	
	//for each other argument see if is valid option indicator
	//if is valid indicator, test option entry for validity and apply to variable
	
	for(i=1;i<argc;i=i+2){
		//printf("checking argument %d\n", i);
		
		if(strcmp(argv[i], "-p") == 0){
			//printf("found a port number\n");
			//portNumber 
			unsigned short n = atoi(argv[i+1]);
			//printf("testing the port given which is %d\n",n);
			//check within correct range
			if(n >= 1024 && n < 65536){
				//printf("port is in range\n");
				port = n;
			}
			else {
				printf("Error: Port number must be between 1024 and 65535.\n");
				exit(1);
			}
		}
		else if (strcmp(argv[i], "-d") == 0){
			//printf("found a document root\n");
			strcpy(docRoot, argv[i+1]);
		}
		else if (strcmp(argv[i], "-l") == 0){
			//printf("found a log file\n");
			strcpy(logFile, argv[i+1]);
		}
		else if (strcmp(argv[i], "-m") == 0){
			//printf("found a mime type file\n");
			strcpy(mimeTypeFile, argv[i+1]);
			
			//add types in file to mimetype struct list
			FILE *xmt;
			int count = 0; 
			char mimeLine[256] = "\0";
			char *mle[MAX_NUM_TOKENS];
			
			if((xmt = fopen(mimeTypeFile, "r")) != NULL){
				//count how many lines in file
				while(!feof(xmt)){
					fgets(mimeLine, 256, xmt);
					count++;
				}
				
				rewind(xmt);
				numMT = numMT + count;
				//realloc for new number needed
				mt = realloc(mt, numMT * sizeof(mime_type));

				//read each line into struct 
				for(int k = 8; k < numMT; k++){
					fgets(mimeLine, 256, xmt);
					tokenise(mimeLine, mle, ",");
					
					//allocate memory for these strings
					mt[k].ext = malloc(sizeof(mle[0]));
					mt[k].type = malloc(sizeof(mle[1]));
					
					sprintf(mt[k].ext, "%s", mle[0]);
					sprintf(mt[k].type, "%s", mle[1]);
				}
				
			}else{
				printf("Error: could not open supplied mime type file\n");
				exit(1);
			}
		}
		else if (strcmp(argv[i], "-f") == 0){
			//printf("found a prefork number\n");
			if((preforks = atoi(argv[i+1])) == 0){
				printf("Error: please enter an integer for number of preforks\n");
				exit(1);
			}
		}
		else{
			printf("Invalid arguments. Please enter in the following form:\n");
			printf("myhttpd [ -p  <port number> ]\n");
			printf("        [ -d  <document root> ]\n");
			printf("        [ -l  <log file> ]\n");
			printf("        [ -m  <file for mime types> ]\n");
			printf("        [ -f  <number of preforks> ]\n");
			exit(1);
		}
		
		//printf("that was %d, on to next\n", i);
		
	} // end for each other argument

	logF = fopen(logFile, "a");
	
	//initialise daemon
	daemon_init();
	
	//printf("setting up socket");
	//fflush(stdout);
	
	//set up listening socket
	if((sd = socket(PF_INET, SOCK_STREAM, 0)) < 0){
		perror("Error: server socket");
		exit(1);
	}
	
	printf("sd is %d\n",sd);
	
	//make server Internet socket address
	bzero((char *)&serverAddress, sizeof(serverAddress)); //replace with memset?
	serverAddress.sin_family = AF_INET;
	serverAddress.sin_port = htons(port);
	serverAddress.sin_addr.s_addr = htonl(INADDR_ANY); //any network interface
	
	//bind address to sd
	if(bind(sd, (struct sockaddr *) &serverAddress, sizeof(serverAddress)) < 0) {
		perror("Error: server bind");
		exit(1);
	}
	
	//printf("start listening\n");
	
	//be a listening socket
	listen(sd, 5);
	
	while(1){
		//prepare preforks to number defined
		while(numChild < preforks){
			//if parent gets notification of child termination numChild is decreased
			if((pid=fork()) <0){// fork failed
				perror("fork");
			
			}else if(pid==0){ //i am the child who is waiting to process something
				numChild = preforks; //so doesn't make own children
				
				//child waits to accept client
				printf("child says: waiting for client, numchild = %d\n",numChild);
				fflush(stdout);
		
				clientAddressLength = sizeof(clientAddress);
				nsd = accept(sd, (struct sockaddr *) &clientAddress, &clientAddressLength);
				if(nsd<0){
					if(errno == EINTR){ //interrupted by SIGCHLD
						continue;
					}
					perror("Error: server accept");
					exit(1);
				}				
		
				//TODO child and client have a deep and meaningful conversation
				//while connection termination not received
				while(1){
					//send to function
					processRequest(nsd);
										
				}
				//TODO conversation ends, this child ends, parent will receive notification and spawn a replacement child
				exit(0);
			
			}else if(pid>0){ //this is a child's PID we are parent
				numChild++;
				printf("parent says: numchildren %d\n", numChild);
				continue;
			}
		}
	}
}


void processRequest(int client){
	//response stuff
	char response[10240] = "\0";
	char status_line[1024] = "\0";
	char response_code[128] = "\0";
	char header_date[31] = "\0";
	char header_server[] = "myhttpd(TM/KM)";
	int hold_cont_length = 0;
	char header_content_length[8] = "\0";
	char header_content_type[32] = "\0";
	char header_close[] = "Connection: close";
	char message_body[9216] = "\0";
	int include_body = 1; //default to true
	char header_location[256] = "\0";
	
	//request stuff
	char request[BUFFER+1];
	char originalRequest[BUFFER+1];
	char *lines[MAX_NUM_TOKENS];
	char *tmpVar[MAX_NUM_TOKENS];
	char client_method[6] = "\0";
	char client_uri[1024] = "\0";
	char client_httpVersion[16] = "\0";
	char client_host[BUFFER] = "\0";
	
	//other stuff
	FILE *fp;
	int found = 0;
		
	//get request 
	read(client, request, BUFFER);
	strcpy(originalRequest, request);
	
	//parse it: split request by CRLF into lines 
	int lineCount=tokenise(request, lines, "\r\n");
	
	//printout for bug checking
	for(int i=0; i<lineCount;i++){
		printf("line %d: %s\n", i, lines[i]);
	}
	
	//now split lines into variables, line[0] always method, uri, version
	tokenise(lines[0], tmpVar, " ");
	strcpy(client_method, tmpVar[0]);
	strcpy(client_uri, tmpVar[1]);
	strcpy(client_httpVersion, tmpVar[2]);
	
	//(other lines) host etc
	for(int i = 1;  i < lineCount; i++ ){
		int items = tokenise(lines[i], tmpVar, " ");
		for (int j = 0; j < items; j++){
			if(strcmp(tmpVar[j], "Host:") == 0){
				strcpy(client_host, tmpVar[j+1]);
			}
		}
	}
	
	//printout for bug check
	printf("parsed method %s\n", client_method);
	printf("parsed uri %s\n", client_uri);
	printf("parsed httpVersion %s\n", client_httpVersion);
	printf("parsed host %s\n", client_host);
	
	//if httpversion not match
	if(strcmp(client_httpVersion, HTTP_VERSION) != 0){
		sprintf(status_line, "%s %s %s", HTTP_VERSION, "505", "HTTP Version Not Supported");
		sprintf(response, "%s\r\n\r\n", status_line);
		logToFile(client_method, client_host, client_uri, "505");
		write(client, response, sizeof(response));
		exit(0);
	}
	
	//if host is not supplied return 400 Bad Request
	if(strcmp(client_host, "") == 0){
		sprintf(status_line, "%s %s %s", HTTP_VERSION, "400", "Bad Request");
		sprintf(response, "%s\r\n\r\n", status_line);
		logToFile(client_method, client_host, client_uri, "400");
		write(client, response, sizeof(response));
		exit(0);
	}
	
	//if GET or HEAD only diff whether including message-body
	if((strcmp(client_method, "GET") == 0) || (strcmp(client_method, "HEAD") ==0)){
		if(strcmp(client_method, "GET")== 0){
			include_body = 1;
		}else{
			include_body = 0;
		}

		//uri could be /, /file.file, /directory (disallow ..) 
		if (strstr(client_uri, "..") != NULL){ //testing if uri contains ..
			//reply is 403 Forbidden
			sprintf(status_line, "%s %s %s", HTTP_VERSION, "403", "Forbidden");
			sprintf(response, "%s\r\n\r\n", status_line);
			logToFile(client_method, client_host, client_uri, "403");
			write(client, response, sizeof(response));
			exit(0);
		}
		
		struct stat path;
		char strPath[1024];
		if(client_uri[0] == '/'){
			strcat(strPath, docRoot);
			strcat(strPath, client_uri);
		} else{
			strcat(strPath, docRoot);
			strcat(strPath, "/");
			strcat(strPath, client_uri);
		}
		if(stat(strPath, &path) < 0){
			perror("Error for path");
			// TODO Return 500 to client
			exit(0);
		}

		if(S_ISREG(path.st_mode)){ //if it's a file 
			char *ret;
			ret = strrchr(client_uri, '.');
			//to get filename 
			char *fn = strrchr(client_uri, '/') + 1;
			char *path = (char*) malloc(256*sizeof(char));
			sprintf(path, "%s%s", docRoot, client_uri);

			if(ret != NULL){
				for(int i = 0; i<numMT; i++){
					if(strcmp(ret, mt[i].ext) == 0){
						found = 1;
						printf("it's a %s file: %s\nfull path: %s\n", mt[i].ext, fn++, path);
						if((fp = fopen(path, "r")) != NULL){
							printf("file opened ok\n");
							fseek(fp, 0, SEEK_END);
							long fsize = ftell(fp);
							rewind(fp);
							fread(message_body, fsize, 1, fp);
							fclose(fp);
							printf("message body is %s\n", message_body);
							sprintf(response_code, "%s %s", "200", "OK");
							sprintf(header_content_type, "%s", mt[i].type);
						}else{
							//file failed to open :(
							sprintf(status_line, "%s %s %s", HTTP_VERSION, "500", "Internal Server Error");	
							sprintf(response, "%s\r\n\r\n", status_line);
							write(client, response, sizeof(response));
							exit(0);
						}
					}
				}//end of do we support it check list

				if(found == 0){
					//file has unsupported extension
					sprintf(status_line, "%s %s %s", HTTP_VERSION, "415", "Unsupported Media Type");	
					sprintf(response, "%s\r\n\r\n", status_line);
					write(client, response, sizeof(response));
					exit(0);
				}
				
				free(path);
			}else{ //no extension
				if((fp = fopen(path, "r")) != NULL){
					printf("file opened ok\n");
					fseek(fp, 0, SEEK_END);
					long fsize = ftell(fp);
					rewind(fp);
					fread(message_body, fsize, 1, fp);
					fclose(fp);
					printf("message body is %s\n", message_body);
					sprintf(response_code, "%s %s", "200", "OK");
					sprintf(header_content_type, "%s", "text/plain");
				}else{
					//file failed to open :(
					sprintf(status_line, "%s %s %s", HTTP_VERSION, "500", "Internal Server Error");	
					sprintf(response, "%s\r\n\r\n", status_line);
					write(client, response, sizeof(response));
					exit(0);
				}
			}
			
		} else if(S_ISDIR(path.st_mode)){
			if((client_uri[(strlen(client_uri)-1)] != '/')){ //something after the final / and it's not a file therefore is a directory redirect to requested adddres + add trailing /
				sprintf(response_code, "%s %s", "301", "Moved Permanently");
				strcpy(header_location, client_uri);
				strcat(header_location, "/");
				
			}else if(client_uri[(strlen(client_uri)-1)] == '/') { //it's ending in /, a directory
				//try to open various default index files
				char dirname[256];
				sprintf(dirname, "%s%s", docRoot, client_uri);
				printf("dirname is %s\n", dirname);
				
				if(findFile(dirname, "index.html")){
					printf("redirect to client_uri + index.html\n");
					sprintf(response_code, "%s %s", "301", "Moved Permanently");
					strcpy(header_location, client_uri);
					strcat(header_location, "index.html");
				}
				else if(findFile(dirname, "index.htm")){
					printf("redirect to client_uri + index.htm\n");
					sprintf(response_code, "%s %s", "301", "Moved Permanently");
					strcpy(header_location, client_uri);
					strcat(header_location, "index.htm");
				}
				else if(findFile(dirname, "default.htm")){
					printf("redirect to client_uri + default.htm\n");
					sprintf(response_code, "%s %s", "301", "Moved Permanently");
					strcpy(header_location, client_uri);
					strcat(header_location, "default.htm");
				}else{
					// otherwise directory listing
					DIR * dp;
					struct dirent *direntp;
					int i = 0;
					char dirLine[256][256];
					
					if((dp = opendir(dirname)) == NULL){
						sprintf(status_line, "%s %s %s", HTTP_VERSION, "500", "Internal Server Error");	
						sprintf(response, "%s\r\n\r\n", status_line);
						logToFile(client_method, client_host, client_uri, "500");
						write(client, response, sizeof(response));
						exit(0);
					}
					
					sprintf(response_code, "%s %s", "200", "OK");
					sprintf(header_content_type, "%s", "text/plain");
					
					while((direntp=readdir(dp)) != NULL){
						sprintf(dirLine[i], "%s\n", direntp->d_name);
						i++;
					}
		
					for(int j = 0; j<i; j++){
						strcat(message_body, dirLine[j]);
					}
				}
			}
		}else{
			//all failed, server sad - set response codes appropriately
			sprintf(status_line, "%s %s %s", HTTP_VERSION, "500", "Internal Server Error");	
			sprintf(response, "%s\r\n\r\n", status_line);
			logToFile(client_method, client_host, client_uri, "500");
			write(client, response, sizeof(response));
			exit(0);
		}
		
		//build response 
		sprintf(status_line, "%s %s\r\n", HTTP_VERSION, response_code);
		strcpy(header_date, getHDate());
		//content type, set elsewhere
		//message body, set elsewhere (include if flag true)
		hold_cont_length = strlen(message_body);
		sprintf(header_content_length, "%d", hold_cont_length);
		
		
		//combine it all 
		if(include_body){
			sprintf(response, "%sDate: %s\r\nServer: %s\r\nContent-Type: %s\r\nContent-Length: %s\r\nLocation: %s\r\n\r\n%s", status_line, header_date, header_server, header_content_type, header_content_length, header_location, message_body);
			
		}else{
			sprintf(response, "%sDate: %s\r\nServer: %s\r\nContent-Type: %s\r\nContent-Length: %s\r\nLocation: %s\r\n\r\n", status_line, header_date, header_server, header_content_type, header_content_length, header_location);
		}
		
		//send header and requested file in content taking into account MIME type
		//Response: Status-Line = HTTP-Version SP Status-Code SP Reason-Phrase CRLF
		//Status-Codes 1xx info, 2xx success, 3xx redirect, 4xx client error, 5xx server error
		//Other headers: Date (Fri, 31 Dec 1999 23:59:59 GMT), Server, Content_Length, Content-Type

	
		logToFile(client_method, client_host, client_uri, response_code);	
		//send response
		write(client, response, sizeof(response));
				
		exit(0);
		
	}else if(strcmp(client_method, "TRACE") == 0){
		sprintf(status_line, "%s %s %s", HTTP_VERSION, "200", "OK");

		strcpy(header_date, getHDate());
		strcpy(header_content_type, "message/http");
		sprintf(header_content_length, "%ld", strlen(originalRequest));

		strncpy(message_body, originalRequest, strlen(originalRequest));

		sprintf(response, "%s\r\nDate: %s\r\nServer: %s\r\n%s\r\nContent-Type: %s\r\nContent-Length: %s\r\n\r\n%s", status_line, header_date, header_server, header_close, header_content_type, header_content_length, message_body);

		logToFile(client_method, client_host, client_uri, "200");
		write(client, response, strlen(response));
		
		exit(0);
		
	}else{
		//has requested method that is not implemented
		sprintf(status_line, "%s %s %s", HTTP_VERSION, "501", "Not Implemented");
		sprintf(response, "%s\r\n\r\n", status_line);

		logToFile(client_method, client_host, client_uri, "501");
		write(client, response, sizeof(response));
		exit(0);
	}
	
	
}

int findFile(char* dir, char* fn){
	DIR *dp;
	struct dirent *direntp;

	int found = 0;
	
	if ((dp = opendir(dir)) == NULL){
		printf("Error opening directory %s\n", dir);
		return(-1);
	}
	
	while((direntp=readdir(dp)) != NULL){
		if(strcmp(direntp->d_name, fn) == 0){
			found = 1;
		}
	}
	
	closedir(dp);
	
	return found;
}

char* getHDate(){
	//https://www.tutorialspoint.com/c_standard_library/c_function_gmtime.htm
	time_t rawtime;
	struct tm *info;
	char day[4];
	char mon[4];
	int year;
	
	char *date = (char *) malloc(sizeof(char) * 31);
		
	time(&rawtime);
	info = gmtime(&rawtime);
	
	switch(info->tm_wday) {
		case 0: sprintf(day, "%s", "Sun"); break;
		case 1: sprintf(day, "%s", "Mon"); break;
		case 2: sprintf(day, "%s", "Tue"); break;
		case 3: sprintf(day, "%s", "Wed"); break;
		case 4: sprintf(day, "%s", "Thu"); break;
		case 5: sprintf(day, "%s", "Fri"); break;
		case 6: sprintf(day, "%s", "Sat"); break;	
	}
	
	switch(info->tm_mon){
		case 0: sprintf(mon, "%s", "Jan"); break;
		case 1: sprintf(mon, "%s", "Feb"); break;
		case 2: sprintf(mon, "%s", "Mar"); break;
		case 3: sprintf(mon, "%s", "Apr"); break;
		case 4: sprintf(mon, "%s", "May"); break;
		case 5: sprintf(mon, "%s", "Jun"); break;
		case 6: sprintf(mon, "%s", "Jul"); break;
		case 7: sprintf(mon, "%s", "Aug"); break;
		case 8: sprintf(mon, "%s", "Sep"); break;
		case 9: sprintf(mon, "%s", "Oct"); break;
		case 10: sprintf(mon, "%s", "Nov"); break;
		case 11: sprintf(mon, "%s", "Dec"); break;
	}
	
	year = (info->tm_year) + 1900;
	
	//aiming for Fri, 31 Dec 1999 23:59:59 GMT
	sprintf(date, "%s, %d %s %d %d:%d:%d GMT", day, info->tm_mday, mon, year, info->tm_hour, info->tm_min, info->tm_sec);

	return date;
}

void daemon_init(void)
{       
     pid_t   pid;
     struct sigaction act;

     if ( (pid = fork()) < 0) {
          perror("fork"); 
		  exit(1); 
     } else if (pid > 0) {
          printf("Please jot down this number for future killing frenzy: %d\n", pid);
          exit(0); //parent ends
     }

     // child continues 
     setsid();  // session leader 
     chdir("/"); // change working directory 
     umask(0); // clear file mode creation mask 

     /* catch SIGCHLD to remove zombies from system */
     act.sa_handler = claim_children; /* use reliable signal */
     sigemptyset(&act.sa_mask);       /* not to block other signals */
     act.sa_flags   = SA_NOCLDSTOP;   /* not catch stopped children */
     sigaction(SIGCHLD,(struct sigaction *)&act,(struct sigaction *)0);
     /* note: a less than perfect method is to use 
              signal(SIGCHLD, claim_children);
     */
}

void claim_children()
{
     pid_t pid=1;
     
     while (pid>0) { /* claim as many zombies as we can */
         pid = waitpid(0, (int *)0, WNOHANG); 
		 numChild = preforks - 1;
     } 
}

void logToFile(char *method, char *host, char *resourceRequest, char *statusCode)
{
	char logMsg[1024];
	sprintf(logMsg, "Date: %s;Method: %s;Host: %s;Resource: %s; StatusCode: %s\n", getHDate(), method, host, resourceRequest, statusCode);
	write(fileno(logF), logMsg, strlen(logMsg));
}
